<?php

namespace Aim\ManageCheckout\Block\Customer;

class Wishlist extends \Magento\Wishlist\Block\Customer\Wishlist
{
    /**
     * @return Wishlist
     */
    protected function _prepareLayout()
    {
        $parent = parent::_prepareLayout();

        $parent->pageConfig->getTitle()->set(__('My Account'));

        return $parent;
    }
}
