<?php

namespace Aim\ManageCheckout\Block\Checkout;

use Magento\Framework\View\Element\Template;

class BackToCart extends Template
{

    /**
     * @var Template\Context
     */
    private $context;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->context = $context;
    }

    /**
     * @return string
     */
    public function getCartUrl(): string
    {
        return $this->context->getUrlBuilder()->getUrl('checkout/cart/index');
    }
}
