<?php

namespace Aim\ManageCheckout\Block\Checkout\Cart;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Shipping
 * @package Aim\ManageCheckout\Block\Checkout\Cart
 */
class Shipping extends Template
{

    /**
     * @var Context
     */
    private $context;

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);

        $this->context = $context;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return (bool)$this->context->getScopeConfig()->getValue(
            'shipping/visibility',
            ScopeInterface::SCOPE_STORE
        );
    }
}
