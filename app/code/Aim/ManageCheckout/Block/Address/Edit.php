<?php

namespace Aim\ManageCheckout\Block\Address;

use Magento\Customer\Block\Address\Edit as MageEdit;
use Magento\Framework\Exception\LocalizedException;

class Edit extends MageEdit
{

    /**
     * @return Edit
     * @throws LocalizedException
     */
    protected function _prepareLayout()
    {
        $block = parent::_prepareLayout();

        $block->getLayout()->getBlock('page.main.title')->setPageTitle(__('Address Book'));

        return $block;
    }
}
