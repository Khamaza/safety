<?php

namespace Aim\ManageCheckout\Block;

use Magento\Checkout\Model\CompositeConfigProvider;
use Magento\Framework\View\Element\Template;

class ConfigProvider extends Template
{

    /**
     * @var Template\Context
     */
    private $context;
    /**
     * @var CompositeConfigProvider
     */
    private $configProvider;
    /**
     * @var array
     */
    private $data;

    public function __construct(
        Template\Context $context,
        CompositeConfigProvider $configProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->context = $context;
        $this->configProvider = $configProvider;
        $this->data = $data;
    }

    /**
     * @return bool|string
     * @since 100.2.0
     */
    public function getSerializedCheckoutConfig()
    {
        return json_encode($this->getCheckoutConfig(), JSON_HEX_TAG);
    }

    /**
     * Retrieve checkout configuration
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function getCheckoutConfig()
    {
        return $this->configProvider->getConfig();
    }
}

