<?php

namespace Aim\ManageCheckout\Plugin;

use Magento\Customer\Block\Account\Dashboard\Info;

class NewsLetterExpanderPlugin
{

    /**
     * @param Info $info
     * @param $result
     *
     * @return bool
     */
    public function afterIsNewsletterEnabled(Info $info, $result): bool
    {
        return false;
    }
}
