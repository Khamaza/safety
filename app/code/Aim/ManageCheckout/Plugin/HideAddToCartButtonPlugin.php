<?php

namespace Aim\ManageCheckout\Plugin;

use Aim\ManageCheckout\Helper\Customer;
use Magento\Catalog\Model\Product;

/**
 * Class HideAddToCartButton
 * @package Aim\ManageCheckout\Plugin
 */
class HideAddToCartButtonPlugin
{
    /**
     * @var Customer
     */
    private $customerSession;

    /**
     * @param Customer $customerSession
     */
    public function __construct(Customer $customerSession)
    {
        $this->customerSession = $customerSession;
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    public function afterIsSaleable(Product $product): bool
    {
        return $this->customerSession->isLoggedIn();
    }
}
