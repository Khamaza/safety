<?php

namespace Aim\ManageCheckout\Plugin;

use Aim\ManageCheckout\Helper\Customer;
use Magento\Customer\Controller\Account\Create;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;

class ControllerAccountCreatePlugin
{

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var RedirectFactory
     */
    private $resultRedirectFactory;

    /**
     * ControllerAccountCreatePlugin constructor.
     * @param Customer $customer
     * @param RedirectFactory $resultRedirectFactory
     */
    public function __construct(Customer $customer, RedirectFactory $resultRedirectFactory)
    {
        $this->customer = $customer;
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    /**
     * @param $controller
     * @param $result
     * @return Redirect
     */
    public function afterExecute(Create $controller, $result)
    {
        if (!$this->customer->isEnabledRegistration()) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');

            return $resultRedirect;
        }

        return $result;
    }
}
