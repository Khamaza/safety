<?php

namespace Aim\ManageCheckout\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Framework\App\Action\Action;
use Magento\Checkout\Model\Session;
use Magento\Framework\Api\Search\SearchCriteriaInterfaceFactory;
use \Magento\Framework\Api\SortOrderFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Swatches\Model\Plugin\ProductImage;

class LastOrderedItemsExpanderPlugin
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Image
     */
    private $imageHelper;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaInterfaceFactory
     */
    private $searchCriteriaFactory;

    /**
     * @var SortOrderFactory
     */
    private $sortOrderFactory;

    /**
     * @var ListProduct
     */
    private $listProduct;

    /**
     * LastOrderedItemsExpanderPlugin constructor.
     * @param Session $checkoutSession
     * @param SearchCriteriaInterfaceFactory $searchCriteriaFactory
     * @param ProductRepositoryInterface $productRepository
     * @param SortOrderFactory $sortOrderFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param Image $imageHelper
     * @param ListProduct $listProduct
     */
    public function __construct(
        Session $checkoutSession,
        SearchCriteriaInterfaceFactory $searchCriteriaFactory,
        ProductRepositoryInterface $productRepository,
        SortOrderFactory $sortOrderFactory,
        OrderRepositoryInterface $orderRepository,
        Image $imageHelper,
        ListProduct $listProduct
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaFactory = $searchCriteriaFactory;
        $this->sortOrderFactory = $sortOrderFactory;
        $this->listProduct = $listProduct;
    }

    /**
     * @param $customer
     * @param $result
     * @throws NoSuchEntityException
     *
     * @return array
     */
    public function afterGetSectionsData($customer, $result): array
    {
        if (isset($result['last-ordered-items'], $result['last-ordered-items']['items']) && !empty($result['last-ordered-items']['items'])) {
            $searchCriteria = $this->searchCriteriaFactory
                ->create()
                ->setPageSize(1)
                ->setSortOrders([
                $this->sortOrderFactory->create()->setField('entity_id')->setDirection('DESC')
            ]);
            if ($this->orderRepository->getList($searchCriteria)->getTotalCount()) {
                $items = $this->orderRepository->getList($searchCriteria)->getFirstItem()->getItems();
                $settings = [];
                foreach ($items as $item) {
                    $product = $this->productRepository->getById($item->getProductId());
                    if ($product) {
                        $settings['images'][$item->getId()] = $this->imageHelper
                            ->init($product, ProductImage::CATEGORY_PAGE_GRID_LOCATION)
                            ->constrainOnly(FALSE)
                            ->keepAspectRatio(TRUE)
                            ->keepFrame(FALSE)
                            ->resize(400)
                            ->getUrl();
                        $settings['params'][$item->getId()] = $this->listProduct->getAddToCartPostParams($product);
                        $settings['prices'][$item->getId()] = $this->listProduct->getProductPriceHtml($product, FinalPrice::PRICE_CODE);
                    }
                }
                if (!empty($settings)) {
                    foreach ($result['last-ordered-items']['items'] as $index => &$productInfo) {
                        if (array_key_exists($productInfo['id'], $settings['images'])) {
                            $productInfo['image'] = $settings['images'][$productInfo['id']];
                            $productInfo['action'] = $settings['params'][$productInfo['id']]['action'];
                            $productInfo['product_id'] = $settings['params'][$productInfo['id']]['data']['product'];
                            $productInfo['value'] = $settings['params'][$productInfo['id']]['data'][Action::PARAM_NAME_URL_ENCODED];
                            $productInfo['price'] = $settings['prices'][$productInfo['id']];
                        } else {
                            $productInfo['image'] = null;
                            $productInfo['action'] = null;
                            $productInfo['product_id'] = null;
                            $productInfo['value'] = null;
                            $productInfo['price'] = null;
                        }
                    }
                }
            }
        }

        return $result;
    }
}
