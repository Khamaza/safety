<?php

namespace Aim\ManageCheckout\Plugin;

use Magento\Checkout\Block\Cart\Totals;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class JsLayoutManagementPlugin
{

    const DISABLED_COMPONENTS = [
        'tax',
        'shipping',
        'discount',
        'tax',
        'weee',
        'vertex-messages'
    ];

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Totals $totals
     * @param $result
     *
     * @return string
     */
    public function afterGetJsLayout(Totals $totals, $result): string
    {
        $jsLayout = json_decode($result ,1);
        foreach ($jsLayout['components']['block-totals']['children'] as $name => $child) {
            if ($this->componentsIsDisabled() && in_array($name, self::DISABLED_COMPONENTS)) {
                $jsLayout['components']['block-totals']['children'][$name]['config']['componentDisabled'] = true;
            }
        }

        return json_encode($jsLayout, JSON_HEX_TAG);
    }

    /**
     * @return bool
     */
    private function componentsIsDisabled(): bool
    {
        return !(bool)$this->scopeConfig->getValue(
            'shipping/visibility',
            ScopeInterface::SCOPE_STORE
        );
    }
}
