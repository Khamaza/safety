<?php

namespace Aim\ManageCheckout\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Webapi\Rest\Request;

class AddDeliveryNotesToOrderObserver implements ObserverInterface
{

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        $data = json_decode($this->request->getContent(),1);
        if (!empty($data['additional']['deliveryNotes'])) {
            $observer->getEvent()->getOrder()->setDeliveryNotes($data['additional']['deliveryNotes']);
        }
    }
}
