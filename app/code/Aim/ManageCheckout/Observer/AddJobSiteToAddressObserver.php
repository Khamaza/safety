<?php

namespace Aim\ManageCheckout\Observer;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\Data\AddressExtensionFactory;

class AddJobSiteToAddressObserver implements ObserverInterface
{

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var AddressExtensionFactory
     */
    private $extensionFactory;

    /**
     * AddJobSiteToAddressObserver constructor.
     * @param RequestInterface $request
     * @param AddressExtensionFactory $extensionFactory
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        RequestInterface $request,
        AddressExtensionFactory $extensionFactory,
        AddressRepositoryInterface $addressRepository
    ) {
        $this->request = $request;
        $this->addressRepository = $addressRepository;
        $this->extensionFactory = $extensionFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $address = $observer->getEvent()->getCustomerAddress();
        $jobSite = $this->request->getPost()['job_site'];
        if ($jobSite) {
            $address->setJobSite($jobSite);
        }
    }
}
