<?php

namespace Aim\ManageCheckout\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Zend_Validate_Exception;

class InstallData implements InstallDataInterface
{
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var AttributeSetFactory
     */
    private $setFactory;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * InstallData constructor.
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $setFactory
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $setFactory,
        AttributeRepositoryInterface $attributeRepository
    )
    {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->setFactory = $setFactory;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws LocalizedException
     * @throws StateException
     * @throws Zend_Validate_Exception
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerAddressEntity = $customerSetup->getEavConfig()->getEntityType('customer_address');
        $attributeSetId = $customerAddressEntity->getDefaultAttributeSetId();

        $attributeSet = $this->setFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $jobSite = 'job_site';

        $customerSetup->addAttribute(\Magento\Customer\Api\AddressMetadataInterface::ENTITY_TYPE_ADDRESS, $jobSite, [
            'type' => 'varchar',
            'label' => 'Job Site',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 100,
            'position' => 100,
            'system' => false,
            'group' => 'General',
            'global' => true,
            'visible_on_front' => true,
        ]);

        $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'job_site')
            ->addData([
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
            ]);
        $attribute->setData('used_in_forms',
            ['customer_address_edit', 'adminhtml_checkout', 'adminhtml_customer', 'adminhtml_customer_address']);

        $this->attributeRepository->save($attribute);
    }
}
