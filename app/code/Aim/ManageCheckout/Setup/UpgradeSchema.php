<?php

namespace Aim\ManageCheckout\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $installer = $setup;

        $installer->startSetup();

        $quote = 'quote';
        $orderTable = 'sales_order';

        $installer->getConnection()
            ->addColumn(
                $setup->getTable($quote),
                'delivery_notes',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Delivery Notes Attribute'
                ]
            );
        //Order table
        $installer->getConnection()
            ->addColumn(
                $setup->getTable($orderTable),
                'delivery_notes',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' =>'Delivery Notes Attribute'
                ]
            );

        $installer->endSetup();

    }
}
