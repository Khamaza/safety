<?php

namespace Aim\ManageCheckout\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    /**
     * @return bool
     */
    public function paymentIsEnabled(): bool
    {
        return (bool)$this->scopeConfig
            ->getValue('account/payment/information', ScopeInterface::SCOPE_STORE);
    }
}
