<?php

namespace Aim\ManageCheckout\Helper;

use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Wishlist extends AbstractHelper
{

    /**
     * @var Context
     */
    private $context;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteria;

    /**
     * Wishlist constructor.
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param SearchCriteriaBuilder $searchCriteria
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        SearchCriteriaBuilder $searchCriteria
    )
    {
        parent::__construct($context);
        $this->context = $context;
        $this->productRepository = $productRepository;
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * @param array $ids
     * @return ProductSearchResultsInterface
     */
    public function getList(array $ids)
    {
        $searchCriteria = $this->searchCriteria->addFilters([
            (new Filter())->setField('entity_id')
                ->setConditionType('in')
                ->setValue(implode(',', $ids))
        ])->create();

        return $this->productRepository->getList($searchCriteria);
    }
}
