<?php


namespace Aim\ManageCheckout\Helper;

use Magento\Customer\Model\ResourceModel\Address\Collection;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class Address extends AbstractHelper
{

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Context
     */
    private $context;

    /**
     * Address constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, CollectionFactory $collectionFactory)
    {
        parent::__construct($context);
        $this->context = $context;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return Collection
     * @throws LocalizedException
     */
    public function getList()
    {
        return $this->collectionFactory->create()
            ->addAttributeToSelect('job_site')
            ->setOrder('job_site')
            ->load();
    }
}
