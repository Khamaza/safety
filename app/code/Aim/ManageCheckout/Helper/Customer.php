<?php

namespace Aim\ManageCheckout\Helper;

use Magento\Customer\Model\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Customer extends AbstractHelper
{
    const XML_PATH_GENERAL_CUSTOMER_REGISTRATION = 'customer/create_account/enable_registration';

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    private $httpContext;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Http\Context $httpContext
    ) {
        parent::__construct($context);
        $this->httpContext = $httpContext;
    }

    /**
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        return (bool)$this->httpContext->getValue(Context::CONTEXT_AUTH);
    }

    /**
     * @return bool
     */
    public function isEnabledRegistration()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_GENERAL_CUSTOMER_REGISTRATION,
            ScopeInterface::SCOPE_STORE
        );
    }
}
