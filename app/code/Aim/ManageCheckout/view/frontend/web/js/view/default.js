define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Aim_ManageCheckout/js/view/summary/place-order',
    'Magento_Checkout/js/action/set-shipping-information',
], function (
    $,
    Component,
    quote,
    placeOrder,
    information
) {
    'use strict';

    return Component.extend({

        isInitialized: false,

        /** @inheritdoc */
        initialize: function () {

            this._super();

            var ctx = this,
                defaultShippingMethod = {
                    amount: 0,
                    available: true,
                    base_amount: 0,
                    carrier_code: "freeshipping",
                    carrier_title: "Free Shipping",
                    error_message: "",
                    method_code: "freeshipping",
                    method_title: "Free",
                    price_excl_tax: 0,
                    price_incl_tax: 0
                },
                defaultPaymentMethod = {
                    method: "checkmo"
                }

            quote.shippingAddress.subscribe(function (value) {
                quote.shippingMethod(defaultShippingMethod);
                if (!this.isInitialized) {
                    information().done(function () {
                        quote.paymentMethod(defaultPaymentMethod);
                        placeOrder().isVisible(true);
                        placeOrder().isDisabled(false);
                        ctx.isInitialized = true;
                    });
                }
                quote.billingAddress(value);
            })

            return this;
        }
    });
});
