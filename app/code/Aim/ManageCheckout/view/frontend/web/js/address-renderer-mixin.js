define(['jquery'],
    function ($) {
        'use strict';

        return function (Component) {
            return Component.extend({
                defaults: {
                    template: 'Aim_ManageCheckout/shipping-address/address-renderer/default'
                },
            });
        }
    });
