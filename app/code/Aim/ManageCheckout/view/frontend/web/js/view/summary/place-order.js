define([
    'ko',
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/action/place-order',
    'Magento_Checkout/js/action/redirect-on-success',
    'Magento_Ui/js/model/messages',
    'Magento_Checkout/js/model/quote'
], function (
    ko,
    $,
    Component,
    placeOrderAction,
    redirectOnSuccessAction,
    Messages,
    quote
) {
    'use strict';

    return Component.extend({

        isVisible: ko.observable(false),
        isDisabled: ko.observable(true),

        /** @inheritdoc */
        initialize: function () {
            this._super();

            return this;
        },

        placeOrder: function () {
            $('#shipping-method-buttons-container button[data-role=opc-continue]').trigger('click');
            if (!this.isDisabled()) {
                this.getPlaceOrderDeferredObject()
                    .done(
                        function () {
                            redirectOnSuccessAction.execute();
                        }
                    );

            }
        },

        getPlaceOrderDeferredObject: function () {
            var messageContainer = new Messages();

            return $.when(
                placeOrderAction(quote.paymentMethod(), messageContainer)
            );
        },
    });
});
