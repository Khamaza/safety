define([
    'jquery',
    'ko',
    'Magento_Ui/js/form/form'
], function (
    $,
    ko,
    Component
) {
    'use strict';

    return Component.extend({

        isUpdated: ko.observable(false),
        text: ko.observable(''),


        /** @inheritdoc */
        initialize: function () {
            this._super();

            return this;
        },

        action: function() {
          return this.isUpdated() ? 'Edit' : 'Add';
        },

        addChangeNotes: function () {
            if (this.text().length) {
                this.isUpdated(!this.isUpdated());
            }
        }
    });
});
