define(['jquery', 'Aim_ManageCheckout/js/view/summary/place-order'],
    function ($, placeOrder) {
        'use strict';

        return function (Component) {
            return Component.extend({
                validateShippingInformation: function () {
                    var result = this._super();
                    placeOrder().isDisabled(!result);

                    return false;
                }
            });
        }
    });
