var config = {
    map :{
        "*" : {
            'Magento_Checkout/js/view/summary/abstract-total': "Aim_ManageCheckout/js/view/summary/totals",
            'Magento_Checkout/js/action/place-order': "Aim_ManageCheckout/js/action/place-order"
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Aim_ManageCheckout/js/shipping-mixin': true
            },
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': {
                'Aim_ManageCheckout/js/address-renderer-mixin': true
            }
        }
    }
}
