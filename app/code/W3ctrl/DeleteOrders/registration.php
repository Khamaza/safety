<?php
/**
 * W3ctrl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the W3ctrl.com license that is
 * available at this URL:
 * https://www.W3ctrl.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * @category W3ctrl
 * @package W3ctrl_DeleteOrders
 * @author         W3ctrl
 * @copyright      (c) W3ctrl Services 2019
 * @email          dev@w3ctrl.com
 * @website        http://www.w3ctrl.com
 * @modified       Fri Jul 19 12:42:18 2019
 * @file_extname   registration.php
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'W3ctrl_DeleteOrders',
    __DIR__
);
