<?php
/**
 * W3ctrl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the W3ctrl.com license that is
 * available at this URL:
 * https://www.W3ctrl.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * @category W3ctrl
 * @package W3ctrl_DeleteOrders
 * @author         W3ctrl
 * @copyright      (c) W3ctrl Services 2019
 * @email          dev@w3ctrl.com
 * @website        http://www.w3ctrl.com
 * @modified       Fri Jul 19 12:44:03 2019
 * @file_extname   Delete.php
 */

namespace W3ctrl\DeleteOrders\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use W3ctrl\DeleteOrders\Model\ResourceModel\Orders\CollectionFactory;
/**
 * Class Delete
 *
 * @package W3ctrl\DeleteOrders\Controller\Adminhtml\Order
 */
class Delete extends Action
{
    /**
     * @var ordersCollectionFactory
     */
    public $ordersCollectionFactory = null;

       /**
     * Delete constructor.
     * @param ordersCollectionFactory $ordersCollectionFactory
     * @param Context $context
     */
    public function __construct(Context $context, CollectionFactory $ordersCollectionFactory)
    {
        $this->ordersCollectionFactory = $ordersCollectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $path = "sales/order/index";

        $params = $this->getRequest()->getParams();

        $ids = [];

        if (isset($params['excluded']) && $params['excluded'] == "false") {
            $ids[] = '*';
        } else {
            if (isset($params['selected'])) {
                if (is_array($params['selected'])) {
                    $ids = $params['selected'];
                } else {
                    $ids = [$params['selected']];
                }
            }
        }

        $countDeleteOrder = 0;
        if (count($ids) > 0) {
            $collection = $this->ordersCollectionFactory->create();
            if ($ids[0] == '*') {
                $countDeleteOrder = $collection->deleteAll();
                $this->messageManager->addSuccess($countDeleteOrder . __('All orders have been successfully deleted'));
            } else {
                foreach ($ids as $id) {
                    $collection->deleteOrder($id);
                    $countDeleteOrder++;
                }
                $this->messageManager->addSuccess($countDeleteOrder . __(' order(s) successfully deleted'));
            }
        } else {
            $this->messageManager->addError(__('Unable to delete orders.'));
        }
        return $this->resultRedirectFactory->create()->setPath($path, []);
    }
}
