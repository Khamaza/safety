<?php
/**
 * W3ctrl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the W3ctrl.com license that is
 * available at this URL:
 * https://www.W3ctrl.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * @category W3ctrl
 * @package W3ctrl_DeleteOrders
 * @author         W3ctrl
 * @copyright      (c) W3ctrl Services 2019
 * @email          dev@w3ctrl.com
 * @website        http://www.w3ctrl.com
 * @modified       Sat Jul 20 07:43:12 2019
 * @file_extname   AddDeleteAction.php
 */

namespace W3ctrl\DeleteOrders\Plugin\Order;
use Magento\Ui\Component\MassAction;
/**
 * Class AddDeleteAction
 * @package W3ctrl\DeleteOrders\Plugin\Order
 */
class AddDeleteAction
{
    /**
     * @var Data
     */
    /**
     * @var _scopeConfig
     * @var _authorization
     * @var XML_PATH_DELETE_ORDERS
     */
    protected $_scopeConfig;
    protected $_authorization;
    const XML_PATH_DELETE_ORDERS = 'delete_order/delete_order_status/enable';
    /**
     * AddDeleteAction constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param AuthorizationInterface $authorization
     */
    public function __construct(
     \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
     \Magento\Framework\AuthorizationInterface $authorization
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_authorization = $authorization;

    }

    /**
     * @param MassAction $object
     * @param $result
     * @return mixed
     */
    public function afterGetChildComponents(MassAction $object, $result)
    {
        if (!isset($result['action_delete'])) {
            return $result;
        }

        if (!$this->_authorization->isAllowed('Magento_Sales::delete') || !$this->getStatus()) {
            unset($result['action_delete']);
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
     $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

     return $this->_scopeConfig->getValue(self::XML_PATH_DELETE_ORDERS, $storeScope);


     }
}
