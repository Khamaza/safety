<?php
/**
 * W3ctrl
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the W3ctrl.com license that is
 * available at this URL:
 * https://www.W3ctrl.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * @category W3ctrl
 * @package W3ctrl_DeleteOrders
 * @author         W3ctrl
 * @copyright      (c) W3ctrl Services 2019
 * @email          dev@w3ctrl.com
 * @website        http://www.w3ctrl.com
 * @modified       Sat Jul 20 07:43:31 2019
 * @file_extname   AddDeleteButton.php
 */

namespace W3ctrl\DeleteOrders\Plugin\Order;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\ObjectManagerInterface;
/**
 * Class AddDeleteButton
 * @package W3ctrl\DeleteOrders\Plugin\Order
 */
class AddDeleteButton
{
    /**
     * @var Data
     */
    /**
     * @var _objectManager
     * @var _backendUrl
     * @var _scopeConfig
     * @var _authorization
     * @var XML_PATH_DELETE_ORDERS
     */
    protected $_objectManager;
    protected $_backendUrl;
    protected $_scopeConfig;
    protected $_authorization;
    const XML_PATH_DELETE_ORDERS = 'delete_order/delete_order_status/enable';
    /**
     * AddDeleteButton constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param AuthorizationInterface $authorization
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        UrlInterface $backendUrl,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\AuthorizationInterface $authorization
    ) {
        $this->_objectManager = $objectManager;
        $this->_backendUrl = $backendUrl;
        $this->_scopeConfig = $scopeConfig;
        $this->_authorization = $authorization;
    }

    /**
     * @param View $object
     * @return null
     */public function beforeSetLayout( \Magento\Sales\Block\Adminhtml\Order\View $object )
    {
        if($this->getStatus() && $this->_authorization->isAllowed('Magento_Sales::delete')) {

            $actionURL = $this->_backendUrl->getUrl('deleteorders/order/delete/', ['selected' =>$object->getOrderId()]);
        $message = __('Are you sure you want to delete this order?');
        $object->addButton(
            'delete_order',
            [
                'label' => __('Delete'),
                'onclick' => "confirmSetLocation('{$message}', '{$actionURL}')",
                'class' => 'delete_order'
            ]
        );

        return null;
       }
    }
    /**
     * @return mixed
     */
     public function getStatus() {
     $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

     return $this->_scopeConfig->getValue(self::XML_PATH_DELETE_ORDERS, $storeScope);


     }

}
