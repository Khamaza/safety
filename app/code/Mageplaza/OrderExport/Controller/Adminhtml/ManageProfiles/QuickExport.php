<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OrderExport
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\OrderExport\Controller\Adminhtml\ManageProfiles;

use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Mageplaza\OrderExport\Controller\Adminhtml\AbstractManageProfiles;
use Mageplaza\OrderExport\Helper\Data;
use Mageplaza\OrderExport\Model\HistoryFactory;
use Mageplaza\OrderExport\Model\ProfileFactory;

/**
 * Class QuickExport
 * @package Mageplaza\OrderExport\Controller\Adminhtml\ManageProfiles
 */
class QuickExport extends AbstractManageProfiles
{
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $orderFactory;

    /**
     * QuickExport constructor.
     *
     * @param ProfileFactory $profileFactory
     * @param Registry $coreRegistry
     * @param Context $context
     * @param FileFactory $fileFactory
     * @param HistoryFactory $historyFactory
     * @param Data $helperData
     * @param Filter $filter
     * @param CollectionFactory $orderFactory
     */
    public function __construct(
        ProfileFactory $profileFactory,
        Registry $coreRegistry,
        Context $context,
        FileFactory $fileFactory,
        HistoryFactory $historyFactory,
        Data $helperData,
        Filter $filter,
        CollectionFactory $orderFactory
    ) {
        $this->fileFactory = $fileFactory;
        $this->historyFactory = $historyFactory;
        $this->helperData = $helperData;
        $this->filter = $filter;
        $this->orderFactory = $orderFactory;

        parent::__construct($profileFactory, $coreRegistry, $context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws Exception
     */
    public function execute()
    {
        $selected = $this->getRequest()->getParam('selected') ?: [];
        $component = $this->filter->getComponent();

        $this->filter->prepareComponent($component);
        $this->filter->applySelectionOnTargetProvider();

        $dataProvider = $component->getContext()->getDataProvider();

        if ($selected !== 'false') {
            $collection = $this->filter->getCollection($this->orderFactory->create());
            $selected = $collection->getAllIds();
        } else {
            $selected = [];
            $items = $dataProvider->getSearchResult()->getItems();
            foreach ($items as $key => $item) {
                $selected[$key] = (int)$item->getId();
            }
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $profile = $this->initProfile();
        $matchingItemIds = $profile->getMatchingItemIds();
        $ids = array_intersect($matchingItemIds, $selected);

        try {
            [$content, $ids] = $this->helperData->generateLiquidTemplate($profile, $ids, false, true);
            $this->helperData->createProfileFile('quickexport', $content);
            $this->historyFactory->create()->addData([
                'profile_id' => $profile->getId(),
                'name' => $profile->getName(),
                'generate_status' => 'Success',
                'type' => 'Quick Export',
                'file' => '',
                'product_count' => count($ids),
                'message' => ''
            ])->save();

            return $this->fileFactory->create(
                'export.' . $this->helperData->getFileType($profile->getFileType()),
                ['type' => 'filename', 'value' => 'mageplaza/order_export/profile/quickexport', 'rm' => true],
                'media'
            );
        } catch (Exception $e) {
            $this->historyFactory->create()->addData([
                'profile_id' => $profile->getId(),
                'name' => $profile->getName(),
                'generate_status' => 'Error',
                'type' => 'Quick Export',
                'file' => '',
                'product_count' => 0,
                'message' => ''
            ])->save();
        }
        $resultRedirect->setPath($this->_redirect->getRefererUrl());

        return $resultRedirect;
    }
}
