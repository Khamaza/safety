<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_OrderExport
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\OrderExport\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Mageplaza\OrderExport\Model\DefaultTemplateFactory;

/**
 * Class UpgradeData
 * @package Mageplaza\OrderExport\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Date model
     *
     * @var DateTime
     */
    public $date;

    /**
     * @var DefaultTemplateFactory
     */
    private $defaultTemplate;

    /**
     * UpgradeData constructor.
     *
     * @param DateTime $date
     * @param DefaultTemplateFactory $defaultTemplate
     */
    public function __construct(
        DateTime $date,
        DefaultTemplateFactory $defaultTemplate
    ) {
        $this->date = $date;
        $this->defaultTemplate = $defaultTemplate;
    }

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $invoiceTemplates = '{"1540893649080_80":{"col_name":"Increment Id","col_type":"attribute","col_attr_val":"increment_id","col_pattern_val":"","col_val":"{{ invoice.increment_id }}"},"1540893657566_566":{"col_name":"Order Id","col_type":"attribute","col_attr_val":"order_id","col_pattern_val":"","col_val":"{{ invoice.order_id }}"},"1540893671326_326":{"col_name":"Order Date","col_type":"attribute","col_attr_val":"order_date","col_pattern_val":"","col_val":"{{ invoice.order_date }}"},"1540893754806_806":{"col_name":"Created At","col_type":"attribute","col_attr_val":"created_at","col_pattern_val":"","col_val":"{{ invoice.created_at }}"},"1540893762149_149":{"col_name":"Store Name","col_type":"attribute","col_attr_val":"store_name","col_pattern_val":"","col_val":"{{ invoice.store_name }}"},"1540893777239_239":{"col_name":"Created At","col_type":"attribute","col_attr_val":"created_at","col_pattern_val":"","col_val":"{{ invoice.created_at }}"},"1540894096661_661":{"col_name":"Customer Firstname","col_type":"attribute","col_attr_val":"customer_firstname","col_pattern_val":"","col_val":"{{ invoice.customer_firstname }}"},"1540894137596_596":{"col_name":"Customer Lastname","col_type":"attribute","col_attr_val":"customer_lastname","col_pattern_val":"","col_val":"{{ invoice.customer_lasttname }}"},"1540894155805_805":{"col_name":"Customer Email","col_type":"attribute","col_attr_val":"customer_email","col_pattern_val":"","col_val":"{{ invoice.customer_email }}"},"1540894234290_290":{"col_name":"Customer Group","col_type":"attribute","col_attr_val":"customer_group","col_pattern_val":"","col_val":"{{ invoice.customer_group }}"},"1540894243101_101":{"col_name":"BillingAddress.Firstname","col_type":"attribute","col_attr_val":"billingAddress.firstname","col_pattern_val":"","col_val":"{{ invoice.billingAddress.firstname }}"},"1540894254635_635":{"col_name":"BillingAddress.Lastname","col_type":"attribute","col_attr_val":"billingAddress.lastname","col_pattern_val":"","col_val":"{{ invoice.billingAddress.lastname }}"},"1540894273292_292":{"col_name":"BillingAddress.Street","col_type":"attribute","col_attr_val":"billingAddress.street","col_pattern_val":"","col_val":"{{ invoice.billingAddress.street }}"},"1540894292410_410":{"col_name":"BillingAddress.City","col_type":"attribute","col_attr_val":"billingAddress.city","col_pattern_val":"","col_val":"{{ invoice.billingAddress.city }}"},"1540894303282_282":{"col_name":"BillingAddress.Postcode","col_type":"attribute","col_attr_val":"billingAddress.postcode","col_pattern_val":"","col_val":"{{ invoice.billingAddress.postcode }}"},"1540894390760_760":{"col_name":"BillingAddress.Telephone","col_type":"attribute","col_attr_val":"billingAddress.telephone","col_pattern_val":"","col_val":"{{ invoice.billingAddress.telephone }}"},"1540894323146_146":{"col_name":"ShippingAddress.Firstname","col_type":"attribute","col_attr_val":"shippingAddress.firstname","col_pattern_val":"","col_val":"{{ invoice.shippingAddress.firstname }}"},"1540894339586_586":{"col_name":"ShippingAddress.Lastname","col_type":"attribute","col_attr_val":"shippingAddress.lastname","col_pattern_val":"","col_val":"{{ invoice.shippingAddress.lastname }}"},"1540894349301_301":{"col_name":"ShippingAddress.Street","col_type":"attribute","col_attr_val":"shippingAddress.street","col_pattern_val":"","col_val":"{{ invoice.shippingAddress.street }}"},"1540894361930_930":{"col_name":"ShippingAddress.City","col_type":"attribute","col_attr_val":"shippingAddress.city","col_pattern_val":"","col_val":"{{ invoice.shippingAddress.city }}"},"1540894368163_163":{"col_name":"ShippingAddress.Postcode","col_type":"attribute","col_attr_val":"shippingAddress.postcode","col_pattern_val":"","col_val":"{{ invoice.shippingAddress.postcode }}"},"1540894381689_689":{"col_name":"ShippingAddress.Telephone","col_type":"attribute","col_attr_val":"shippingAddress.telephone","col_pattern_val":"","col_val":"{{ invoice.shippingAddress.telephone }}"},"1540894412449_449":{"col_name":"Payment Method","col_type":"attribute","col_attr_val":"payment_method","col_pattern_val":"","col_val":"{{ invoice.payment_method }}"},"1540894471908_908":{"col_name":"Shipping Description","col_type":"attribute","col_attr_val":"shipping_description","col_pattern_val":"","col_val":"{{ invoice.shipping_description }}"},"1540894698772_772":{"col_name":"Base Shipping Incl Tax","col_type":"attribute","col_attr_val":"base_shipping_incl_tax","col_pattern_val":"","col_val":"{{ invoice.base_shipping_incl_tax }}"},"1540894727315_315":{"col_name":"Subtotal","col_type":"attribute","col_attr_val":"subtotal","col_pattern_val":"","col_val":"{{ invoice.subtotal }}"},"1540894736608_608":{"col_name":"Tax Amount","col_type":"attribute","col_attr_val":"tax_amount","col_pattern_val":"","col_val":"{{ invoice.tax_amount }}"},"1540894759127_127":{"col_name":"Grand Total","col_type":"attribute","col_attr_val":"grand_total","col_pattern_val":"","col_val":"{{ invoice.grand_total }}"},"1540894764416_416":{"col_type":"item","col_attr_val":"0","col_pattern_val":"","col_val":"","items":{"1540894769736_736":{"name":"Name","value":"name"},"1540894776343_343":{"name":"Sku","value":"sku"},"1540894788356_356":{"name":"Price","value":"price"},"1540894827811_811":{"name":"Qty","value":"qty"},"1540894838014_14":{"name":"Base Row_total","value":"base_row_total"},"1540894843724_724":{"name":"Tax Amount","value":"tax_amount"},"1540894850285_285":{"name":"Discount Amount","value":"discount_amount"},"1540894996810_810":{"name":"Row Total","value":"row_total"}}}}';
            $shipmentTemplates = '{"1540897316858_858":{"col_name":"Increment Id","col_type":"attribute","col_attr_val":"increment_id","col_pattern_val":"","col_val":"{{ shipment.increment_id }}"},"1540897338234_234":{"col_name":"Order Id","col_type":"attribute","col_attr_val":"order_id","col_pattern_val":"","col_val":"{{ shipment.order_id }}"},"1540897352623_623":{"col_name":"Created At","col_type":"attribute","col_attr_val":"created_at","col_pattern_val":"","col_val":"{{ shipment.created_at }}"},"1540897373897_897":{"col_name":"Order Date","col_type":"attribute","col_attr_val":"order_date","col_pattern_val":"","col_val":"{{ shipment.order_date }}"},"1540897379670_670":{"col_name":"Order Status","col_type":"attribute","col_attr_val":"order_status","col_pattern_val":"","col_val":"{{ shipment.order_status }}"},"1540897385199_199":{"col_name":"Store Name","col_type":"attribute","col_attr_val":"store_name","col_pattern_val":"","col_val":"{{ shipment.store_name }}"},"1540897394703_703":{"col_name":"Customer Firstname","col_type":"attribute","col_attr_val":"customer_firstname","col_pattern_val":"","col_val":"{{ shipment.customer_firstname }}"},"1540897402398_398":{"col_name":"Customer Lastname","col_type":"attribute","col_attr_val":"customer_lastname","col_pattern_val":"","col_val":"{{ shipment.customer_lasttname }}"},"1540897409927_927":{"col_name":"Customer Email","col_type":"attribute","col_attr_val":"customer_email","col_pattern_val":"","col_val":"{{ shipment.customer_email }}"},"1540897415967_967":{"col_name":"Customer Group","col_type":"attribute","col_attr_val":"customer_group","col_pattern_val":"","col_val":"{{ shipment.customer_group }}"},"1540897424303_303":{"col_name":"BillingAddress.Firstname","col_type":"attribute","col_attr_val":"billingAddress.firstname","col_pattern_val":"","col_val":"{{ shipment.billingAddress.firstname }}"},"1540897434413_413":{"col_name":"BillingAddress.Lastname","col_type":"attribute","col_attr_val":"billingAddress.lastname","col_pattern_val":"","col_val":"{{ shipment.billingAddress.lastname }}"},"1540897456928_928":{"col_name":"BillingAddress.Street","col_type":"attribute","col_attr_val":"billingAddress.street","col_pattern_val":"","col_val":"{{ shipment.billingAddress.street }}"},"1540897486859_859":{"col_name":"BillingAddress.City","col_type":"attribute","col_attr_val":"billingAddress.city","col_pattern_val":"","col_val":"{{ shipment.billingAddress.city }}"},"1540897495660_660":{"col_name":"BillingAddress.Postcode","col_type":"attribute","col_attr_val":"billingAddress.postcode","col_pattern_val":"","col_val":"{{ shipment.billingAddress.postcode }}"},"1540897505762_762":{"col_name":"BillingAddress.Telephone","col_type":"attribute","col_attr_val":"billingAddress.telephone","col_pattern_val":"","col_val":"{{ shipment.billingAddress.telephone }}"},"1540897525404_404":{"col_name":"ShippingAddress.Firstname","col_type":"attribute","col_attr_val":"shippingAddress.firstname","col_pattern_val":"","col_val":"{{ shipment.shippingAddress.firstname }}"},"1540897535444_444":{"col_name":"ShippingAddress.Lastname","col_type":"attribute","col_attr_val":"shippingAddress.lastname","col_pattern_val":"","col_val":"{{ shipment.shippingAddress.lastname }}"},"1540897543163_163":{"col_name":"ShippingAddress.Street","col_type":"attribute","col_attr_val":"shippingAddress.street","col_pattern_val":"","col_val":"{{ shipment.shippingAddress.street }}"},"1540897549057_57":{"col_name":"ShippingAddress.City","col_type":"attribute","col_attr_val":"shippingAddress.city","col_pattern_val":"","col_val":"{{ shipment.shippingAddress.city }}"},"1540897557482_482":{"col_name":"ShippingAddress.Postcode","col_type":"attribute","col_attr_val":"shippingAddress.postcode","col_pattern_val":"","col_val":"{{ shipment.shippingAddress.postcode }}"},"1540897564900_900":{"col_name":"ShippingAddress.Telephone","col_type":"attribute","col_attr_val":"shippingAddress.telephone","col_pattern_val":"","col_val":"{{ shipment.shippingAddress.telephone }}"},"1540897577586_586":{"col_name":"Payment Method","col_type":"attribute","col_attr_val":"payment_method","col_pattern_val":"","col_val":"{{ shipment.payment_method }}"},"1540897582433_433":{"col_name":"Shipping Description","col_type":"attribute","col_attr_val":"shipping_description","col_pattern_val":"","col_val":"{{ shipment.shipping_description }}"},"1540897592709_709":{"col_name":"Base Shipping Incl Tax","col_type":"attribute","col_attr_val":"base_shipping_incl_tax","col_pattern_val":"","col_val":"{{ shipment.base_shipping_incl_tax }}"},"1540897735431_431":{"col_name":"Total Qty","col_type":"attribute","col_attr_val":"total_qty","col_pattern_val":"","col_val":"{{ shipment.total_qty }}"},"1540897746548_548":{"col_type":"item","col_attr_val":"0","col_pattern_val":"","col_val":"","items":{"1540897750645_645":{"name":"Name","value":"name"},"1540897763145_145":{"name":"Sku","value":"sku"},"1540897767609_609":{"name":"Qty","value":"qty"}}}}';
            $creditmemoTemplates = '{"1540900677032_32":{"col_name":"Increment Id","col_type":"attribute","col_attr_val":"increment_id","col_pattern_val":"","col_val":"{{ creditmemo.increment_id }}"},"1540900685438_438":{"col_name":"Order Id","col_type":"attribute","col_attr_val":"order_id","col_pattern_val":"","col_val":"{{ creditmemo.order_id }}"},"1540900693672_672":{"col_name":"Created At","col_type":"attribute","col_attr_val":"created_at","col_pattern_val":"","col_val":"{{ creditmemo.created_at }}"},"1540900700421_421":{"col_name":"Order Date","col_type":"attribute","col_attr_val":"order_date","col_pattern_val":"","col_val":"{{ creditmemo.order_date }}"},"1540900706208_208":{"col_name":"Order Status","col_type":"attribute","col_attr_val":"order_status","col_pattern_val":"","col_val":"{{ creditmemo.order_status }}"},"1540900802104_104":{"col_name":"Store Name","col_type":"attribute","col_attr_val":"store_name","col_pattern_val":"","col_val":"{{ creditmemo.store_name }}"},"1540900820079_79":{"col_name":"Customer Firstname","col_type":"attribute","col_attr_val":"customer_firstname","col_pattern_val":"","col_val":"{{ creditmemo.customer_firstname }}"},"1540900828373_373":{"col_name":"Customer Lastname","col_type":"attribute","col_attr_val":"customer_lastname","col_pattern_val":"","col_val":"{{ creditmemo.customer_lasttname }}"},"1540900834151_151":{"col_name":"Customer Email","col_type":"attribute","col_attr_val":"customer_email","col_pattern_val":"","col_val":"{{ creditmemo.customer_email }}"},"1540900842414_414":{"col_name":"Customer Group","col_type":"attribute","col_attr_val":"customer_group","col_pattern_val":"","col_val":"{{ creditmemo.customer_group }}"},"1540900860261_261":{"col_name":"BillingAddress.Firstname","col_type":"attribute","col_attr_val":"billingAddress.firstname","col_pattern_val":"","col_val":"{{ creditmemo.billingAddress.firstname }}"},"1540900874993_993":{"col_name":"BillingAddress.Lastname","col_type":"attribute","col_attr_val":"billingAddress.lastname","col_pattern_val":"","col_val":"{{ creditmemo.billingAddress.lastname }}"},"1540900939415_415":{"col_name":"BillingAddress.Street","col_type":"attribute","col_attr_val":"billingAddress.street","col_pattern_val":"","col_val":"{{ creditmemo.billingAddress.street }}"},"1540900946868_868":{"col_name":"BillingAddress.City","col_type":"attribute","col_attr_val":"billingAddress.city","col_pattern_val":"","col_val":"{{ creditmemo.billingAddress.city }}"},"1540900952484_484":{"col_name":"BillingAddress.Postcode","col_type":"attribute","col_attr_val":"billingAddress.postcode","col_pattern_val":"","col_val":"{{ creditmemo.billingAddress.postcode }}"},"1540900960743_743":{"col_name":"BillingAddress.Telephone","col_type":"attribute","col_attr_val":"billingAddress.telephone","col_pattern_val":"","col_val":"{{ creditmemo.billingAddress.telephone }}"},"1540900966749_749":{"col_name":"ShippingAddress.Firstname","col_type":"attribute","col_attr_val":"shippingAddress.firstname","col_pattern_val":"","col_val":"{{ creditmemo.shippingAddress.firstname }}"},"1540901004563_563":{"col_name":"ShippingAddress.Lastname","col_type":"attribute","col_attr_val":"shippingAddress.lastname","col_pattern_val":"","col_val":"{{ creditmemo.shippingAddress.lastname }}"},"1540901016083_83":{"col_name":"ShippingAddress.Street","col_type":"attribute","col_attr_val":"shippingAddress.street","col_pattern_val":"","col_val":"{{ creditmemo.shippingAddress.street }}"},"1540901024345_345":{"col_name":"ShippingAddress.City","col_type":"attribute","col_attr_val":"shippingAddress.city","col_pattern_val":"","col_val":"{{ creditmemo.shippingAddress.city }}"},"1540901034165_165":{"col_name":"ShippingAddress.Postcode","col_type":"attribute","col_attr_val":"shippingAddress.postcode","col_pattern_val":"","col_val":"{{ creditmemo.shippingAddress.postcode }}"},"1540901043259_259":{"col_name":"ShippingAddress.Telephone","col_type":"attribute","col_attr_val":"shippingAddress.telephone","col_pattern_val":"","col_val":"{{ creditmemo.shippingAddress.telephone }}"},"1540901052582_582":{"col_name":"Payment Method","col_type":"attribute","col_attr_val":"payment_method","col_pattern_val":"","col_val":"{{ creditmemo.payment_method }}"},"1540901056600_600":{"col_name":"Shipping Description","col_type":"attribute","col_attr_val":"shipping_description","col_pattern_val":"","col_val":"{{ creditmemo.shipping_description }}"},"1540901065753_753":{"col_name":"Base Shipping Incl Tax","col_type":"attribute","col_attr_val":"base_shipping_incl_tax","col_pattern_val":"","col_val":"{{ creditmemo.base_shipping_incl_tax }}"},"1540901092748_748":{"col_name":"Subtotal","col_type":"attribute","col_attr_val":"subtotal","col_pattern_val":"","col_val":"{{ creditmemo.subtotal }}"},"1540901103946_946":{"col_name":"Adjustment","col_type":"attribute","col_attr_val":"adjustment","col_pattern_val":"","col_val":"{{ creditmemo.adjustment }}"},"1540901362585_585":{"col_name":"Adjustment Negative","col_type":"attribute","col_attr_val":"adjustment_negative","col_pattern_val":"","col_val":"{{ creditmemo.adjustment_negative }}"},"1540901375766_766":{"col_name":"Tax Amount","col_type":"attribute","col_attr_val":"tax_amount","col_pattern_val":"","col_val":"{{ creditmemo.tax_amount }}"},"1540901381360_360":{"col_name":"Grand Total","col_type":"attribute","col_attr_val":"grand_total","col_pattern_val":"","col_val":"{{ creditmemo.grand_total }}"},"1540901389638_638":{"col_type":"item","col_attr_val":"0","col_pattern_val":"","col_val":"","items":{"1540901392569_569":{"name":"Name","value":"name"},"1540901398883_883":{"name":"Sku","value":"sku"},"1540901403955_955":{"name":"Price","value":"price"},"1540901412895_895":{"name":"Qty","value":"qty"},"1540901423042_42":{"name":"Base Row_total","value":"base_row_total"},"1540901430401_401":{"name":"Tax Amount","value":"tax_amount"},"1540901439443_443":{"name":"Discount Amount","value":"discount_amount"},"1540901445514_514":{"name":"Row Total","value":"row_total"}}}}';

            $invoiceUpdates = ['invoice_tsv', 'invoice_txt', 'invoice_csv'];
            $shipmentUpdates = ['shipment_tsv', 'shipment_txt', 'shipment_csv'];
            $creditmemoUpdates = ['creditmemo_tsv', 'creditmemo_txt', 'creditmemo_csv'];

            $collection = $this->defaultTemplate->create()->getCollection();

            foreach ($collection as $item) {
                if (in_array($item->getName(), $invoiceUpdates, true)) {
                    $item->setData('fields_list', $invoiceTemplates);
                }
                if (in_array($item->getName(), $shipmentUpdates, true)) {
                    $item->setData('fields_list', $shipmentTemplates);
                }
                if (in_array($item->getName(), $creditmemoUpdates, true)) {
                    $item->setData('fields_list', $creditmemoTemplates);
                }
                $item->save();
            }
        }
        $installer->endSetup();
    }
}
