<?php
/**
 * @author ShippingEasy Team
 * @copyright Copyright (c) 2016 ShippingEasy (http://www.shippingeasy.com)
 * @package ShippingEasy_Fulfilment
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'ShippingEasy_Fulfilment',
    __DIR__
);
