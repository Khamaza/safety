<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_AddMultipleProducts
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\AddMultipleProducts\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $productImageHelper;

    /**
     * @var \Magento\Tax\Model\CalculationFactory
     */
    protected $calculationFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Helper\Image $productImageHelper,
        \Magento\Tax\Model\CalculationFactory $calculationFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->productImageHelper = $productImageHelper;
        $this->calculationFactory = $calculationFactory;
        $this->storeManager = $storeManager;
        parent::__construct($context, $this->scopeConfig);
    }

    /**
     * @param string $path
     * @param string $scope
     * @param int $id
     * @return bool
     */
    public function hasFlagConfig($path, $id = null)
    {
        return $this->scopeConfig->isSetFlag($path, ScopeInterface::SCOPE_STORE, $id);
    }

    /**
     * @param string $path
     * @param string $scope
     * @param int $id
     * @return string
     */
    public function getValueConfig($path, $id = null)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $id);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/general/active');
    }

    /**
     * @return array|bool
     */
    public function getCustomerGroup()
    {
        $customer_group = $this->getValueConfig('ajaxmuntiplecart/general/active_for_customer_group');
        if ($customer_group != '') {
            return explode(',', $customer_group);
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function displayAddmunltiple()
    {
        return $this->getValueConfig('ajaxmuntiplecart/general/display_addmuntiple');
    }

    /**
     * @return mixed
     */
    public function defaultQty()
    {
        return $this->getValueConfig('ajaxmuntiplecart/general/default_qty');
    }

    /**
     * @return mixed
     */
    public function positionButton()
    {
        return $this->getValueConfig('ajaxmuntiplecart/button_grid/position_button');
    }

    /**
     * @return mixed
     */
    public function showTotal()
    {
        return $this->getValueConfig('ajaxmuntiplecart/button_grid/display_total');
    }

    /**
     * @return bool
     */
    public function showSelectProduct()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/button_grid/show_select_product');
    }

    /**
     * @return bool
     */
    public function showStick()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/button_grid/show_stick');
    }

    /**
     * @return mixed
     */
    public function backGroundStick()
    {
        return $this->getValueConfig('ajaxmuntiplecart/button_grid/background_stick');
    }

    /**
     * @return bool
     */
    public function isShowProductImage()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/success_popup/product_image');
    }

    /**
     * @return mixed
     */
    public function getImageSizesg()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/product_image_size_sg');
    }

    /**
     * @return mixed
     */
    public function getImageSizemt()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/product_image_size_mt');
    }

    /**
     * @return mixed
     */
    public function getImageSizeer()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/product_image_size_er');
    }

    /**
     * @return mixed
     */
    public function getItemonslide()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/item_on_slide');
    }

    /**
     * @return mixed
     */
    public function getSlidemove()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/slide_move');
    }

    /**
     * @return mixed
     */
    public function getSlidespeed()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/slide_speed');
    }

    /**
     * @return mixed
     */
    public function getSlideauto()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/slide_auto');
    }

    /**
     * @return bool
     */
    public function isShowProductPrice()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/success_popup/product_price');
    }

    /**
     * @return bool
     */
    public function isShowContinueBtn()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/success_popup/continue_button');
    }

    /**
     * @return mixed
     */
    public function getCountDownActive()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/active_countdown');
    }

    /**
     * @return mixed
     */
    public function getCountDownTime()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/countdown_time');
    }

    /**
     * @return bool
     */
    public function isShowCartInfo()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/success_popup/mini_cart');
    }

    /**
     * @return bool
     */
    public function isShowCheckoutLink()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/success_popup/mini_checkout');
    }

    /**
     * @return bool
     */
    public function isShowSuggestBlock()
    {
        return $this->hasFlagConfig('ajaxmuntiplecart/success_popup/suggest_product');
    }

    /**
     * @return mixed
     */
    public function getSuggestLimit()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup/suggest_limit');
    }

    /**
     * @return mixed|string
     */
    public function getBtnTextColor()
    {
        $color = $this->getValueConfig('ajaxmuntiplecart/success_popup_design/button_text_color');

        $color = ($color == '') ? '' : $color;
        return $color;
    }

    /**
     * @return mixed
     */
    public function getBtnContinueText()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup_design/continue_text');
    }

    /**
     * @return mixed|string
     */
    public function getBtnContinueBackground()
    {
        $backGround = $this->getValueConfig('ajaxmuntiplecart/success_popup_design/continue');

        $backGround = ($backGround == '') ? '' : $backGround;
        return $backGround;
    }

    /**
     * @return mixed|string
     */
    public function getBtnContinueHover()
    {
        $hover = $this->getValueConfig('ajaxmuntiplecart/success_popup_design/continue_hover');

        $hover = ($hover == '') ? '' : $hover;
        return $hover;
    }

    /**
     * @return mixed
     */
    public function getBtnViewcartText()
    {
        return $this->getValueConfig('ajaxmuntiplecart/success_popup_design/viewcart_text');
    }

    /**
     * @return mixed|string
     */
    public function getBtnViewcartBackground()
    {
        $backGround = $this->getValueConfig('ajaxmuntiplecart/success_popup_design/viewcart');

        $backGround = ($backGround == '') ? '' : $backGround;
        return $backGround;
    }

    /**
     * @return mixed|string
     */
    public function getBtnViewcartHover()
    {
        $hover = $this->getValueConfig('ajaxmuntiplecart/success_popup_design/viewcart_hover');

        $hover = ($hover == '') ? '' : $hover;
        return $hover;
    }

    /**
     * @return mixed|string
     */
    public function getTextbuttonaddmt()
    {
        $button_text_addmt = $this->getValueConfig('ajaxmuntiplecart/success_popup_design/button_text_addmt');

        $button_text_addmt = ($button_text_addmt == '') ? '' : $button_text_addmt;
        return $button_text_addmt;
    }

    /**
     * @param $product
     * @param $imageId
     * @param $size
     * @return \Magento\Catalog\Helper\Image
     */
    public function resizeImage($product, $imageId, $size)
    {
        $resizedImage = $this->productImageHelper
                           ->init($product, $imageId)
                           ->constrainOnly(true)
                           ->keepAspectRatio(true)
                           ->keepTransparency(true)
                           ->keepFrame(false)
                           ->resize($size, $size);
        return $resizedImage;
    }

    /**
     * @param $store
     * @param $taxClassId
     * @return float
     */
    public function getPercent($store, $taxClassId)
    {
        $taxCalculation = $this->calculationFactory->create();
        $request = $taxCalculation->getRateRequest(null, null, null, $store);
        $percent = $taxCalculation->getRate($request->setProductClassId($taxClassId));
        return $percent;
    }

    /**
     * @param $product
     * @return float|int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function taxRate($product)
    {
        $store = $this->storeManager->getStore();
        $taxClassId = $product->getTaxClassId();
        $percent = $this->getPercent($store, $taxClassId);
        return ($percent/100);
    }
}
