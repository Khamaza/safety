<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutCustomField
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutCustomField\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Visitor Observer
 */
class AddColumn implements ObserverInterface
{
    protected $resource;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * @param EventObserver $model
     */
    public function execute(EventObserver $model)
    {
        $connection = $this->resource->getConnection();
        $attribute = $model->getAttribute();
        if (!$connection->tableColumnExists(
            $this->resource->getTableName('bss_checkout_attribute_order_grid_view'),
            $attribute->getAttributeCode()
        )) {
            $connection->addColumn(
                $this->resource->getTableName('bss_checkout_attribute_order_grid_view'),
                $attribute->getAttributeCode(),
                'text'
            );
        }
    }
}
