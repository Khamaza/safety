/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutCustomField
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
define(
    [
        'jquery',
        'uiRegistry',
        'mage/validation'
    ],
    function ($, registry) {
        'use strict';

        return {

            /**
             * Validate checkout agreements
             *
             * @returns {Boolean}
             */
            validate: function () {
                var source = registry.get('checkoutProvider');
                if(typeof source.get('paymentBeforemethods') === "undefined" && $('[name^=paymentBeforemethods]').length == 0)
                    return true;
                source.set('params.invalid', false);
                source.trigger('paymentBeforemethods.data.validate');
                return !source.get('params.invalid');
            }
        };
    }
);
