<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutCustomField
 * @author     Extension Team
 * @copyright  Copyright (c) 2018-2019 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutCustomField\Helper;

use Magento\Store\Model\StoreManagerInterface as StoreId;
use Magento\Framework\Json\Helper\Data as JsonHelper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_SECURE_IN_FRONTEND = 'web/secure/use_in_frontend';

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var string
     */
    protected $_configSectionId = 'custom_field';

    /**
     * @var StoreId
     */
    protected $storeId;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_resigtry;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * Data constructor.
     * @param \Magento\Framework\Registry $resigtry
     * @param \Magento\Framework\App\Helper\Context $context
     * @param JsonHelper $jsonHelper
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param StoreId $storeId
     */
    public function __construct(
        \Magento\Framework\Registry $resigtry,
        \Magento\Framework\App\Helper\Context $context,
        JsonHelper $jsonHelper,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        StoreId $storeId
    ) {
        $this->_resigtry = $resigtry;
        $this->jsonHelper = $jsonHelper;
        $this->productMetadata = $productMetadata;
        $this->storeId = $storeId;
        $this->logger = $context->getLogger();
        parent::__construct($context);
    }

    /**
     * @return int
     */
    public function IntlDateFormatterShort()
    {
        return \IntlDateFormatter::SHORT;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function checkSerialize($data)
    {
        if ($data == null || $data == '') {
            return [];
        }
        $version = $this->productMetadata->getVersion();
        $checkVersion = version_compare($version, '2.2.0', '>=');

        if ($checkVersion) {
            $additionalData = json_decode($data, true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \InvalidArgumentException('Unable to unserialize value.');
            }
        } else {
            $additionalData = unserialize($data);
        }
        return $additionalData;
    }

    /**
     * @param $path
     * @param null $store
     * @param null $scope
     * @return mixed
     */
    public function getConfig($path, $store = null, $scope = null)
    {
        if ($scope === null) {
            $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        }
        return $this->scopeConfig->getValue($path, $scope, $store);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function moduleEnabled($store = null, $scope = null)
    {
        return (bool)$this->getConfig($this->_configSectionId.'/general/enable', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return string
     */
    public function getTitle($store = null, $scope = null)
    {
        return (string)$this->getConfig($this->_configSectionId.'/general/title', $store, $scope);
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    protected function hasPdfView($store = null, $scope = null)
    {
        return (bool)$this->getConfig($this->_configSectionId.'/general/pdf_view', $store, $scope);
    }

    /**
     * @return bool
     */
    protected function hasEmailView()
    {
        return (bool)$this->getConfig($this->_configSectionId.'/general/email_view');
    }

    /**
     * @param null $store
     * @param null $scope
     * @return bool
     */
    public function showInPdf($store = null, $scope = null)
    {
        return $this->moduleEnabled($store, $scope) && $this->hasPdfView($store, $scope);
    }

    /**
     * @return bool
     */
    public function showInEmail()
    {
        return $this->moduleEnabled() && $this->hasEmailView();
    }

    /**
     * @param $json
     * @return string
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function getVariableEmailHtml($json)
    {
        $html = '';
        if ($this->showInEmail() && $json && $this->hasDataCustomFieldEmail($json)) {
                $html = '<h3>' . $this->getTitle() . '</h3>';
                $bssCustomfield = $this->jsonHelper->jsonDecode($json);
            foreach ($bssCustomfield as $key => $field) {
                if ($field['show_in_email'] == '1') {
                    $fieldText = $field['frontend_label'] . ': ';
                    if ((is_array($field['value']) && !empty($field['value'])) || $field['value'] != "") {
                        $html .= "<span>" . $this->setFieldTextHtml($field, $fieldText) . "</span><br/>";
                    }
                }
            }
        }
        return $html;
    }

    /**
     * @param $field
     * @param $fieldText
     * @return string
     */
    private function setFieldTextHtml($field, $fieldText)
    {
        if (is_array($field['value'])) {
            foreach ($field['value'] as $value) {
                $fieldText .= $value . ",";
            }
        } else {
            $fieldText .= $field['value'];
        }
        return $fieldText;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrentStoreId()
    {
        return $this->storeId->getStore()->getId();
    }

    /**
     * @param $customField
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function hasDataCustomFieldOrder($customField)
    {
        $customField = $this->jsonHelper->jsonDecode($customField);
        foreach ($customField as $key => $field) {
            if ($field['show_in_order'] == '1') {
                if (is_array($field['value']) && !empty($field['value'])) {
                    return true;
                } elseif ($field['value'] != "") {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $customField
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function hasDataCustomFieldPdf($customField)
    {
        $customField = $this->jsonHelper->jsonDecode($customField);
        foreach ($customField as $key => $field) {
            if ($field['show_in_email'] == '1') {
                if (is_array($field['value']) && !empty($field['value'])) {
                    return true;
                } elseif ($field['value'] != "") {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $customField
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function hasDataCustomFieldEmail($customField)
    {
        $customField = $this->jsonHelper->jsonDecode($customField);
        foreach ($customField as $key => $field) {
            if ($field['show_in_pdf'] == '1') {
                if (is_array($field['value']) && !empty($field['value'])) {
                    return true;
                } elseif ($field['value'] != "") {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param $attributeCode
     * @param $customField
     * @return bool
     */
    public function isValueOrderByAttributeCode($attributeCode, $customField)
    {
        $customField = $this->jsonHelper->jsonDecode($customField);
        if (isset($customField[$attributeCode])) {
            return $customField[$attributeCode]['val'];
        }
        return false;
    }

    /**
     * @param string $type
     * @return string
     */
    public function getSaveCustomFieldtoQuote()
    {
        return $this->_getUrl(
            'customfield/express/saveCustomField',
            ['_secure' => true]
        );
    }

    /**
     * @return bool
     */
    public function isFormatDate()
    {
        return $this->getConfig($this->_configSectionId.'/general/date_format') == 1;
    }
}
