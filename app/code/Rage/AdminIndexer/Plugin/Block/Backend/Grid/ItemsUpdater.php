<?php
namespace Rage\AdminIndexer\Plugin\Block\Backend\Grid;

class ItemsUpdater
{
    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $authorization;

    /**
     * @param \Magento\Framework\AuthorizationInterface $authorization
     */
    public function __construct(\Magento\Framework\AuthorizationInterface $authorization)
    {
        $this->authorization = $authorization;
    }

    public function afterUpdate($subject, $result)
    {
        if (false === $this->authorization->isAllowed('Rage_Indexer::reindex')) {
            unset($result['change_mode_reindex']);
        }
        return $result;
    }
}
