<?php
namespace Rage\AdminIndexer\Block\Backend\Grid\Column\Renderer;

class Action extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
    * @var \Magento\Framework\AuthorizationInterface
    */
    protected $authorization;

    /**
     * @param \Magento\Framework\AuthorizationInterface $authorization
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\AuthorizationInterface $authorization,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->authorization = $authorization;
    }

    /**
     * Render indexer action
     *
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row)
    {
        $cell = '-';
        $indexer = $row->getIndexerId();
        if (!empty($indexer) && $this->authorization->isAllowed('Rage_Indexer::reindex')) {
            $reindex_url = $this->getUrl('*/*/reindex', ['indexer_id' => $indexer]);
            $label = __('Reindex Data');
            $cell = '<a href="' . $reindex_url . '">' . $label . '</a>';
        }
        return $cell;
    }
}
