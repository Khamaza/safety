<?php
namespace Rage\AdminIndexer\Controller\Adminhtml\Indexer;

class MassReindex extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $indexerFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory
    ) {
        parent::__construct($context);
        $this->indexerFactory = $indexerFactory;
    }

    /**
     * Turn mview on for the given indexers
     *
     * @return void
     */
    public function execute()
    {
        $indexer_ids = $this->getRequest()->getParam('indexer_ids');

        if (!is_array($indexer_ids)) {
            $this->messageManager->addError(__('Please select one or more indexer(s).'));
        } else {
            try {
                foreach ($indexer_ids as $indexer_id) {
                    $index_data = $this->getIndexerData($indexer_id);
                    $index_data->reindexAll();
                    $this->messageManager->addSuccess(
                        __('%1 re-indexed successfully.', $index_data->getTitle())
                    );
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __("Could not re-index indexer due to " . $e->getMessage())
                );
            }
        }
        $this->_redirect('*/*/list');
    }
    private function getIndexerData($indexer_id)
    {
        $indexer = $this->indexerFactory->create()->load($indexer_id);
        return $indexer;
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Rage_Indexer::reindex');
    }
}
