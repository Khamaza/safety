<?php
namespace Rage\AdminIndexer\Controller\Adminhtml\Indexer;

class Reindex extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Indexer\Model\IndexerFactory
     */
    protected $indexerFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Indexer\Model\IndexerFactory $indexerFactory
    ) {
        parent::__construct($context);
        $this->indexerFactory = $indexerFactory;
    }

    /**
     * Turn mview on for the given indexers
     *
     * @return void
     */
    public function execute()
    {
        $indexer_id = $this->getRequest()->getParam('indexer_id');

        if (!$indexer_id) {
            $this->messageManager->addError(__('Invalid Indexer'));
        } else {
            try {
                $index_data = $this->indexerFactory->create()->load($indexer_id);
                $index_data->reindexAll();

                $this->messageManager->addSuccess(
                    __('%1 re-indexed successfully.', $index_data->getTitle())
                );
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __("Could not re-index indexer due to " . $e->getMessage())
                );
            }
        }
        $this->_redirect('*/*/list');
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Rage_Indexer::reindex');
    }
}
