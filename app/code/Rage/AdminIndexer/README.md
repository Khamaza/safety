OnDemand Indexer
================

By default Magento 2 admin module does not have an option to reindex data on demand. This extension provides an option to re-index available indexes at any time. No need to wait for developer / development team to re-index.

## 1. Installation
Create Directory "Rage/AdminIndexer" on /app/code/
mkdir -p app/code/Rage/AdminIndexer

Extract the Extension Archive on /app/code/Rage/AdminIndexer

## 2. Activation
Run the following command in Magento 2 root folder

php bin/magento module:enable Rage_AdminIndexer
php bin/magento setup:upgrade
php bin/magento cache:clean

## Support
If you encounter any problems or bugs, please email magento.support@whatarage.com
Need help setting up or want to customize this extension to meet your business needs? Please email magento.support@whatarage.com

